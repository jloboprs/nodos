'use strict';

var _ = require('underscore'),
    nodos = require('./nodos'),
    dummy = require('./empresas/dummy');

var dummyNodos = {
    buscarPasajes: function buscarPasajes(tramo) {
        return dummy(tramo);
    }
};

module.exports = _.extend(nodos, dummyNodos);