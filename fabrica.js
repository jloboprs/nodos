'use strict';
var _ = require('underscore');
var moment = require('moment');

var tramo = function tramo(valores) {
    var tramoBase;

    var esValido = function esValido() {
        return tramoBase.origen && tramoBase.destino
            && (!tramoBase.salida || tramoBase.salida instanceof Date)
            && (!tramoBase.llegada || tramoBase.llegada instanceof Date);
    };

    tramoBase = {
        origen: undefined,
        destino: undefined,
        salida: undefined,
        llegada: undefined,
        esValido: esValido,
        constructor: tramo
    };

    return _.extend(tramoBase, valores);
};

var pasaje = function pasaje(valores){
    var pasajeBase;

    var precio = function precio() {
        return pasajeBase.tarifaBase;
    };

    var duracion = function duracion(){
        if (!pasajeBase.salida || !pasajeBase.llegada){
            return undefined;
        }

        var duration = moment.duration(pasajeBase.llegada - pasajeBase.salida, 'milliseconds');

        var horas = parseInt(duration.asHours(), 10),
            minutos = duration.minutes(),
            total = horas + minutos / 60;

        return {
            total: total,
            horas: horas,
            minutos: minutos
        };
    };

    var calidad = function calidad(){
        if (!pasajeBase.servicio || !pasajeBase.servicio.nivel){
            return undefined;
        }

        var sillas = pasajeBase.sillas || 0;
        var duracion = pasajeBase.duracion();
        duracion = duracion ? duracion.total : 0;

        var valor = (pasajeBase.servicio.nivel * 1950 / pasajeBase.precio())
            -(duracion/20) +(sillas/500) + 5;

        return 1 / valor;
    };

    var oferta = function oferta(){
        if (!pasajeBase.servicio || !pasajeBase.servicio.nivel){
            return undefined;
        }

        var sillas = pasajeBase.sillas || 0;
        var duracion = pasajeBase.duracion();
        duracion = duracion ? duracion.total : 0;

        var valor = 70/(pasajeBase.precio()^0.1)
            -(pasajeBase.servicio.nivel / 10)
            -(duracion/20) +(sillas/500);

        return 1 / valor;
    };

    pasajeBase = {
        tarifaBase: 0,
        sillas: undefined,
        llegada: undefined,
        salida: Date.now(),
        operador: {},
        servicio: {},
        precio: precio,
        duracion: duracion,
        calidad: calidad,
        oferta: oferta,
        constructor: pasaje
    };

    return _.extend(pasajeBase, valores);
};

module.exports = {
    tramo: tramo,
    pasaje: pasaje
};