'use strict';
var _ = require('underscore');

var lugares = require('./public/lugares.json');

var obtenerLugar = function obtenerLugar(codigo) {
    return _.find(lugares, function(lugar){
        return lugar.codigo === codigo;
    });
};

var obtenerLugares = function obtenerLugares() {
    return lugares;
};

module.exports = {
    obtenerLugar: obtenerLugar,
    obtenerLugares: obtenerLugares
};