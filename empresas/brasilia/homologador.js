'use strict';

var URI = require('URIjs'),
    moment = require('moment'),
    pasajeBrasilia = require('./pasajeBrasilia'),
    homologaciones = require('./homologaciones.json');

var formatoFecha = 'DD/MM/YYYY',
    formatoHora = 'HH:mm';

var homologarCiudad = function homologarCiudad(ciudad) {
    var ciudadHomologada = homologaciones.ciudades[ciudad.codigo];

    return ciudadHomologada
        ? ciudadHomologada.ciudad.toUpperCase()
        : undefined;
};

var homologarServiciosOperadores = function homologarServiciosOperadores(servicioHomologar, pasaje) {
    var fragmentoOperador = URI(servicioHomologar).filename().toLowerCase();
    var servicio = homologaciones.serviciosOperadores[fragmentoOperador];

    pasaje.operador = servicio ? {nombre: servicio.operador} : undefined;
    pasaje.servicio = {
        nombre: servicio.servicio,
        imagen: servicio.imagen,
        nivel: servicio.nivel,
        caracteristicas: servicio.caracteristicas
    };

    return pasaje;
};

var homologarPrecio = function homologarPrecio(precio) {
    var reg = /(,?(\d)+)+(\.\d+)?/;

    return reg.test(precio) ? parseFloat(reg.exec(precio)[0].replace(/,/g, '')) : 0;
};

var homologarAsiento = function homologarAsiento(asiento) {
    var reg = /\(\s*(\d+)\s*\)/;

    return reg.test(asiento) ? parseInt(reg.exec(asiento)[1], 10) : 0;
};

var homologarFecha = function homologarFecha(fechaStr) {
    var reg = /(\d{1,2}:\d{1,2})(\d{1,2}\/\d{1,2}\/\d{1,4})/,
        fechas = reg.exec(fechaStr),

        fecha = moment(fechas[2], formatoFecha, true),
        hora = moment(fechas[1], formatoHora, true);

    return fecha.hour(hora.hour()).minute(hora.minute()).toDate();
};

var homologarTramo = function homologarTramo(tramo) {
    return {
        salida: moment(tramo.salida).format(formatoFecha),
        llegada: moment(tramo.llegada).format(formatoFecha),
        origen: homologarCiudad(tramo.origen),
        destino: homologarCiudad(tramo.destino)
    };
};

var homologarPasaje = function homologarPasaje(pasaje) {
    var pasajeHomologado = {
        salida: homologarFecha(pasaje.salida),
        llegada: homologarFecha(pasaje.llegada),
        tarifaExtra: homologarPrecio(pasaje.tarifaExtra),
        tarifaSuper: homologarPrecio(pasaje.tarifaSuper),
        tarifaEconomica: homologarPrecio(pasaje.tarifaEconomica),
        tarifaPlena: homologarPrecio(pasaje.tarifaPlena),
        asientos: homologarAsiento(pasaje.asientos)
    };

    pasajeHomologado = homologarServiciosOperadores(pasaje.operador, pasajeHomologado);

    return pasajeBrasilia(pasajeHomologado);
};

var homologador = {
    homologarTramo: homologarTramo,
    homologarPasaje: homologarPasaje
};

module.exports = homologador;