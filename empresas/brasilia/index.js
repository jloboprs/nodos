'use strict';

var q = require('q'),
    URI = require('URIjs'),
    cheerio = require('cheerio'),
    consulta = require('../../lib/consulta'),
    homologador = require('./homologador');

var brasilia = function brasilia(tramo) {
    var urlBase = 'http://186.118.168.234:7777/TiquetePW',
        tramoHomologado,

        obtenerListaVacia = function obtenerListaVacia() {
            return [];
        },

        homologarTramo = function homologarTramo() {
            var defer = q.defer();
            tramoHomologado = homologador.homologarTramo(tramo);

            if (tramoHomologado.origen && tramoHomologado.destino) {
                defer.resolve(tramoHomologado);
            } else {
                defer.reject(obtenerListaVacia);
            }

            return defer.promise;
        },

        consultarSesion = function consultarSesion() {
            var url = URI(urlBase).segment('faces').segment('TIQW001').suffix("xhtml").toString();
            return consulta.head(url).jar().consultar();
        },

        hacerClickEnOrigen = function hacerClickEnOrigen(response) {
            var parametros = {
                    id: 'jforms[tIQW001Controller-compra-origen-0]',
                    value: '',
                    currentField: 'jforms[tIQW001Controller-compra-origen-0]'
                },
                url = URI(urlBase).segment('jforms').segment('clickEvent').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        consultarOrigen = function consultarOrigen(response) {
            var parametros = {
                    beanName: "tIQW001Controller",
                    autocompleteId: 'Origenes',
                    query: tramoHomologado.origen
                },
                url = URI(urlBase).segment('jforms').segment('autocomplete').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        seleccionarOrigen = function seleccionarOrigen(response) {
            var parametros = {
                    beanId: "tIQW001Controller",
                    autocompleteId: 'Origenes',
                    currentField: 'jforms[tIQW001Controller-compra-origen-0]',
                    recordId: 0
                },
                url = URI(urlBase).segment('jforms').segment('autocompleteSelection').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        hacerClickEnDestino = function hacerClickEnDestino(response) {
            var parametros = {
                    id: 'jforms[tIQW001Controller-compra-destino-0]',
                    value: tramoHomologado.origen,
                    currentField: 'jforms[tIQW001Controller-compra-origen-0]'
                },
                url = URI(urlBase).segment('jforms').segment('clickEvent').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        consultarDestino = function consultarDestino(response) {
            var parametros = {
                    beanName: "tIQW001Controller",
                    autocompleteId: 'Destinos',
                    query: tramoHomologado.destino
                },
                url = URI(urlBase).segment('jforms').segment('autocomplete').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        seleccionarDestino = function seleccionarDestino(response) {
            var parametros = {
                    beanId: "tIQW001Controller",
                    autocompleteId: 'Destinos',
                    currentField: 'jforms[tIQW001Controller-compra-destino-0]',
                    recordId: 0
                },
                url = URI(urlBase).segment('jforms').segment('autocompleteSelection').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        hacerClickEnFechaIda = function hacerClickEnFechaIda(response) {
            var parametros = {
                    id: 'jforms[tIQW001Controller-compra-f_ida-0]',
                    value: tramoHomologado.destino,
                    currentField: 'jforms[tIQW001Controller-compra-destino-0]'
                },
                url = URI(urlBase).segment('jforms').segment('clickEvent').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        hacerClickEnIda = function hacerClickEnIda(response) {
            var parametros = {
                    id: 'jforms[tIQW001Controller-compra-i-0]',
                    value: tramoHomologado.salida,
                    currentField: 'jforms[tIQW001Controller-compra-f_ida-0]'
                },
                url = URI(urlBase).segment('jforms').segment('clickEvent').addQuery(parametros).toString();

            return consulta.head(url).jar(response.request._jar).consultar();
        },

        hacerClickEnBuscar = function hacerClickEnBuscar(response) {
            var parametros = {
                    id: 'jforms[tIQW001Controller-compra-buscar-0]',
                    value: 'null',
                    currentField: 'jforms[tIQW001Controller-compra-i-0]'
                },
                url = URI(urlBase).segment('jforms').segment('clickEvent').addQuery(parametros).toString();

            return consulta.get(url).jar(response.request._jar).consultar();
        },

        consultarPasajes = function consultarPasajes(response) {
            var data = JSON.parse(response.body);

            return consulta.get(data.jforms.webShowDocument[0].urlOpen).jar(response.request._jar).consultar();
        },

        cerrarFormulario = function cerrarFormulario(response) {
            var parametros = {
                    beanName: 'tIQW001Controller'
                },
                url = URI(urlBase).segment('jforms').segment('closeForm').addQuery(parametros).toString();

            consulta.head(url).jar(response.request._jar).consultar();

            return response;
        },

        extraerPasajes = function extraerPasajes(response) {
            var $ = cheerio.load(response.body);

            return $('table.table-red > tbody > tr').map(function () {
                var elemento = $(this),

                    pasaje = {
                        ruta: elemento.children(0).text(),
                        salida: elemento.children(1).text(),
                        llegada: elemento.children(2).text(),
                        operador: elemento.children(3).children().attr('src'),
                        tarifaExtra: elemento.children(4).text(),
                        tarifaSuper: elemento.children(5).text(),
                        tarifaEconomica: elemento.children(6).text(),
                        tarifaPlena: elemento.children(7).text(),
                        asientos: elemento.children(7).text()
                    };

                return homologador.homologarPasaje(pasaje);
            }).toArray();
        },

        manejarFlujoAlterno = function manejarFlujoAlterno(dato) {
            if (typeof dato === 'function') {
                return dato();
            }

            return obtenerListaVacia();
        },

        buscarPasajes = function buscarPasajes() {
            return homologarTramo()
                .then(consultarSesion)
                .then(hacerClickEnOrigen)
                .then(consultarOrigen)
                .then(seleccionarOrigen)
                .then(hacerClickEnDestino)
                .then(consultarDestino)
                .then(seleccionarDestino)
                .then(hacerClickEnFechaIda)
                .then(hacerClickEnIda)
                .then(hacerClickEnBuscar)
                .then(consultarPasajes)
                .then(cerrarFormulario)
                .then(extraerPasajes)
                .fail(manejarFlujoAlterno);
        };

    return buscarPasajes();
};

module.exports = brasilia;