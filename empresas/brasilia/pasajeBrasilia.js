'use strict';

var _ = require('underscore');
var fabrica = require('../../fabrica');

var pasajeBrasilia = function pasajeBrasilia(valores){
    var pasaje;

    var precio = function precio() {
        var precios = [pasaje.tarifaExtra, pasaje.tarifaSuper, pasaje.tarifaEconomica];

        return _(precios).reduce(function (economico, precio) {
            return precio && precio < economico ? precio : economico;
        }, pasaje.tarifaPlena);
    };

    pasaje = {
        tarifaExtra: undefined,
        tarifaSuper: undefined,
        tarifaEconomica: undefined,
        tarifaPlena: undefined,
        precio: precio,
        constructor: pasajeBrasilia
    };

    pasaje =_.extend(pasaje, valores);
    pasaje = _.extend(fabrica.pasaje(), pasaje);

    return pasaje;
};

module.exports = pasajeBrasilia;