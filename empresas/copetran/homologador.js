'use strict';

var moment = require('moment'),
    under = require('underscore'),
    pasajeCopetran = require('./pasajeCopetran'),
    homologaciones = require('./homologaciones.json'),

    homologarCiudad = function homologarCiudad(ciudad) {
        var ciudadHomologada = homologaciones.ciudades[ciudad.codigo];
        return ciudadHomologada
            ? ciudadHomologada.code
            : undefined;
    },

    homologarFecha = function homologarFecha(fecha) {
        var fechaHomologada = moment(fecha, 'DD/MM/YY HH:mm', true);

        return fechaHomologada.isValid()
            ? fechaHomologada.toDate()
            : undefined;
    },

    homologarPrecio = function homologarPrecio(precio) {
        var reg = /(.?(\d)+)+(\,\d+)?/;

        return reg.test(precio)
            ? parseFloat(reg.exec(precio)[0].replace(/\./g, ''))
            : 0;
    },

    homologarServicio = function homologarServicio(servicio) {
        return homologaciones.servicios[servicio.toLowerCase()];
    },

    homologarTramo = function homologarTramo(tramo) {
        var salida = moment(tramo.salida),
            llegada = moment(tramo.llegada);

        return {
            origen: homologarCiudad(tramo.origen),
            destino: homologarCiudad(tramo.destino),
            salida: {anio: salida.format('YYYY'), mes: salida.format('MM'), dia: salida.format('DD')},
            llegada: {anio: llegada.format('YYYY'), mes: llegada.format('MM'), dia: llegada.format('DD')}
        };
    },

    homologarPasaje = function homologarPasaje(pasaje) {
        var pasajeHomologado = {
            salida: homologarFecha(pasaje.salida),
            llegada: homologarFecha(pasaje.llegada),
            tarifaBase: homologarPrecio(pasaje.tarifaBase),
            servicio: homologarServicio(pasaje.servicio),
            operador: {nombre: 'Copetran'},
            sillas: parseInt(pasaje.sillas, 10)
        };

        return pasajeCopetran(pasajeHomologado);
    },

    homologarFormulario = function homologarFormulario(formulario, tramo) {
        var formularioHomologado = {},
            formularioConf = homologaciones.formulario,
            filtro = [formularioConf.fechaVueltaAno, formularioConf.fechaVueltaMes, formularioConf.fechaVueltaDia];

        formularioHomologado[formularioConf.origen] = tramo.origen;
        formularioHomologado[formularioConf.destino] = tramo.destino;
        formularioHomologado[formularioConf.fechaIdaAno] = tramo.salida.anio;
        formularioHomologado[formularioConf.fechaIdaMes] = tramo.salida.mes;
        formularioHomologado[formularioConf.fechaIdaDia] = tramo.salida.dia;
        formularioHomologado[formularioConf.ida] = 'radioIda';

        under.reduce(formulario, function (formulario, valor, propiedad) {
            if (valor && filtro.indexOf(propiedad) < 0 && !formulario.hasOwnProperty(propiedad)) {
                formulario[propiedad] = valor;
            }

            return formulario;
        }, formularioHomologado);

        return formularioHomologado;
    },

    homologador = {
        homologarTramo: homologarTramo,
        homologarPasaje: homologarPasaje,
        homologarFormulario: homologarFormulario
    };

module.exports = homologador;