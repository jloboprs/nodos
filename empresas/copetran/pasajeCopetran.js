'use strict';

var _ = require('underscore');
var fabrica = require('../../fabrica');

var pasajeCopetran = function pasajeCopetran(valores){
    var pasaje = {constructor: pasajeCopetran };

    pasaje =_.extend(pasaje, valores);
    pasaje = _.extend(fabrica.pasaje(), pasaje);

    return pasaje;
};

module.exports = pasajeCopetran;