'use strict';

var q = require('q'),
    cheerio = require('cheerio'),
    homologador = require('./homologador'),
    consulta = require('../../lib/consulta');

var copetran = function copetran(tramo) {
    var url = 'https://vonline.copetran.com.co/vo/(S())/MainForm.aspx',
        tramoHomologado,

        obtenerListaVacia = function obtenerListaVacia() {
            return [];
        },

        homologarTramo = function homologarTramo() {
            var defer = q.defer();
            tramoHomologado = homologador.homologarTramo(tramo);

            if (tramoHomologado.origen && tramoHomologado.destino) {
                defer.resolve(tramoHomologado);
            } else {
                defer.reject(obtenerListaVacia);
            }

            return defer.promise;
        },

        consularFormulario = function consularFormulario() {
            return consulta.get(url).jar().consultar();
        },

        extraerFormulario = function extraerFormulario(response) {
            var $ = cheerio.load(response.body),
                formData = {};

            $('form [name]').each(function () {
                var $this = $(this);

                if (['radio', 'checkbox'].indexOf($this.attr('type')) < 0) {
                    formData[$this.attr('name')] = $this.val();
                }

                if ($this.attr('checked')) {
                    formData[$this.attr('name')] = $this.attr('value');
                }
            });

            formData = homologador.homologarFormulario(formData, tramoHomologado);

            return {
                url: response.request.href,
                jar: response.request._jar,
                data: formData
            };
        },

        consularPasajes = function consularPasajes(parametros) {
            return consulta.post(parametros.url, parametros.data).jar(parametros.jar).consultar();
        },

        extraerPasajes = function extraerPasajes(response) {
            var $ = cheerio.load(response.body),
                selector = '#ctl00_ctl00_BodyPlaceHolder_PagePlaceHolder_DataTable_Viajes tr:not(:first-child)';

            return $(selector).map(function () {
                var elemento = $(this),

                    pasajes = {
                        salida: elemento.children(1).text(),
                        llegada: elemento.children(2).text(),
                        tarifaBase: elemento.children(3).text(),
                        sillas: elemento.children(4).text(),
                        servicio: elemento.children(5).text()
                    };

                return homologador.homologarPasaje(pasajes);
            }).toArray();
        },

        manejarFlujoAlterno = function manejarFlujoAlterno(dato) {
            if (typeof dato === 'function') {
                return dato();
            }

            return obtenerListaVacia();
        },

        buscarPasajes = function buscarPasajes() {
            return homologarTramo()
                .then(consularFormulario)
                .then(extraerFormulario)
                .then(consularPasajes)
                .then(extraerPasajes)
                .fail(manejarFlujoAlterno);
        };

    return buscarPasajes();
};

module.exports = copetran;