'use strict';

var moment = require('moment'),
    under = require('underscore'),
    pasajeBolivariano = require('./pasajeBolivariano'),
    homologaciones = require('./homologaciones.json');

var homologarCiudad = function homologarCiudad(ciudad) {
        var ciudadHomologada = homologaciones.ciudades[ciudad.codigo];

        return ciudadHomologada
            ? ciudadHomologada.code
            : undefined;
    },

    homologarOperador = function homologarOperador(operador) {
        if (!operador) {
            return undefined;
        }

        var posibleOperador = under(operador.split(/\s+/))
            .find(function (posibleOperador) {
                return homologaciones.operadores.hasOwnProperty(posibleOperador);
            });

        return homologaciones.operadores[posibleOperador];
    },

    homologarServicio = function homologarServicio(servicio) {
        if (!servicio) {
            return undefined;
        }

        var posibleServicio = under(servicio.split(/\s+/))
            .find(function (posibleServicio) {
                return homologaciones.servicios.hasOwnProperty(posibleServicio);
            });

        return homologaciones.servicios[posibleServicio];
    },

    homologarPrecio = function homologarPrecio(precio) {
        var reg = /(,?(\d)+)+(\.\d+)?/;

        return reg.test(precio)
            ? parseFloat(reg.exec(precio)[0].replace(/,/g, ''))
            : undefined;
    },

    homologarFecha = function homologarFecha(horaHomologar, fechaTramo) {
        var hora = moment(horaHomologar, 'hh:mm A', true);

        return hora.isValid()
            ? new Date(fechaTramo.getFullYear(), fechaTramo.getMonth(), fechaTramo.getDate(), hora.hour(), hora.minute())
            : undefined;
    },

    homologarPasaje = function homologarPasaje(pasaje, tramo) {
        var pasajeHomologado = {
            tarifaIncreible: homologarPrecio(pasaje.tarifaIncreible),
            tarifaChevere: homologarPrecio(pasaje.tarifaChevere),
            tarifaComoda: homologarPrecio(pasaje.tarifaComoda),
            tarifaPrimeraClase: homologarPrecio(pasaje.tarifaPrimeraClase),
            salida: homologarFecha(pasaje.salida, tramo.salida),
            servicio: homologarServicio(pasaje.servicio),
            operador: homologarOperador(pasaje.operador)
        };

        return pasajeBolivariano(pasajeHomologado);
    },

    homologarTramo = function homologarTramo(tramo) {
        var formatoFecha = 'DD.MM.YYYY';

        return {
            salida: moment(tramo.salida).format(formatoFecha),
            llegada: moment(tramo.llegada).format(formatoFecha),
            origen: homologarCiudad(tramo.origen),
            destino: homologarCiudad(tramo.destino)
        };
    };

module.exports = {
    homologarTramo: homologarTramo,
    homologarPasaje: homologarPasaje
};