'use strict';

var _ = require('underscore');
var fabrica = require('../../fabrica');

var pasajeBolivariano = function pasajeBolivariano(valores){
    var pasaje;

    var precio = function precio() {
        var precios = [pasaje.tarifaIncreible, pasaje.tarifaChevere, pasaje.tarifaPrimeraClase];

        return _(precios).reduce(function (economico, precio) {
            return precio && precio < economico ? precio : economico;
        }, pasaje.tarifaComoda);
    };

    pasaje = {
        tarifaIncreible: undefined,
        tarifaChevere: undefined,
        tarifaComoda: undefined,
        tarifaPrimeraClase: undefined,
        precio: precio,
        constructor: pasajeBolivariano
    };

    pasaje =_.extend(pasaje, valores);
    pasaje = _.extend(fabrica.pasaje(), pasaje);

    return pasaje;
};

module.exports = pasajeBolivariano;