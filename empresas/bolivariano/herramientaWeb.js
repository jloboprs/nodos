var guard = {
    depth: 0,
    maxDepth: 20,
    size: 0,
    maxSize: 2000
};

var getFormValue = function (formValues, elemento) {
    if (elemento.attr("type"))
        if (elemento.attr("type") == 'radio' || elemento.attr("type") == 'checkbox')
            if (!elemento.attr("checked"))
                return;
    var name = elemento.attr("name");
    var values = [];
    if ('select-multiple' == elemento.attr("type")) {
        var jLen = elemento.attr("length");
        for (var j = 0; j < jLen; ++j) {
            var option = elemento.attr("options")[j];
            if (true == option.selected)
                values.push(option.value);
        }
    } else {
        values = elemento.val();
    }
    var keyBegin = name.indexOf("[");
    if (0 <= keyBegin) {
        var n = name;
        var k = n.substr(0, n.indexOf("["));
        var a = n.substr(n.indexOf("["));
        if (typeof formValues[k] == 'undefined')
            formValues[k] = [];
        var p = formValues;
        while (a.length != 0) {
            var sa = a.substr(0, a.indexOf("]") + 1);
            a = a.substr(a.indexOf("]") + 1);
            p = p[k];
            k = sa.substr(1, sa.length - 2);
            if (k == "")
                k = p.length;
            if (typeof p[k] == 'undefined')
                p[k] = [];
        }
        p[k] = values;
    } else {
        formValues[name] = values;
    }
};

var objectToXML = function (obj) {
    var aXml = [];
    aXml.push("<xjxobj>");
    for (var key in obj) {
        ++guard.size;
        if (guard.maxSize < guard.size)
            return aXml.join('');
        if ('undefined' != typeof obj[key]) {
            if ("constructor" == key)
                continue;
            if ("function" == typeof (obj[key]))
                continue;
            aXml.push("<e><k>");
            aXml.push(escape(key));
            aXml.push("</k><v>");
            if ("object" == typeof (obj[key])) {
                ++guard.depth;
                if (guard.maxDepth > guard.depth) {
                    try {
                        aXml.push(objectToXML(obj[key]));
                    } catch (e) {}
                }
                --guard.depth;
            } else
                aXml.push(escape(obj[key]));
            aXml.push("</v></e>");
        }
    }
    aXml.push("</xjxobj>");
    return aXml.join('');
};

var escape = function (data) {
    if ('undefined' == typeof data)
        return '';
    if ('string' != typeof (data))
        return data;
    var needCDATA = false;
    if (encodeURIComponent(data) != data) {
        needCDATA = true;
        var segments = data.split("<![CDATA[");
        data = '';
        for (var i = 0; i < segments.length; ++i) {
            var segment = segments[i];
            var fragments = segment.split("]]>");
            segment = '';
            for (var j = 0; j < fragments.length; ++j) {
                if (0 != j)
                    segment += ']]]]><![CDATA[>';
                segment += fragments[j];
            }
            if (0 != i)
                data += '<![]]><![CDATA[CDATA[';
            data += segment;
        }
    }
    if (needCDATA)
        data = '<![CDATA[' + data + ']]>';
    return data;
};

module.exports = {
    escape: escape,
    objectToXML: objectToXML,
    getFormValue: getFormValue
};