'use strict';

var q = require('q'),
    cheerio = require('cheerio'),
    under = require('underscore'),
    consulta = require('../../lib/consulta'),
    template = require('URIjs/src/URITemplate'),
    herramienta = require('./herramientaWeb'),
    homologador = require('./homologador');

var bolivariano = function bolivariano(tramo) {
    var urlPasajes,

        obtenerListaVacia = function obtenerListaVacia() {
            return [];
        },

        configurarParametros = function configurarParametros() {
            var urlFormulario,
                urlParametros,
                defer = q.defer(),
                tramoHomologado = homologador.homologarTramo(tramo);

            if (tramoHomologado.origen && tramoHomologado.destino) {
                urlParametros = template('reservas/buscar/{origen}/{destino}/{salida}/{llegada}/ida')
                    .expand(tramoHomologado);

                urlFormulario = 'http://www.bolivariano.com.co/' + urlParametros;
                urlPasajes = [urlFormulario, '?q=', urlParametros].join('');

                defer.resolve(urlFormulario);
            } else {
                defer.reject(obtenerListaVacia);
            }

            return defer.promise;
        },

        consultarFormulario = function consultarFormulario(url) {
            return consulta(url);
        },

        obtienerFormulario = function obtienerFormulario(response) {
            var $ = cheerio.load(response.body),
                elementos = $('#tiquetes2-nueva-reserva [name]'),
                formValues = {};

            elementos.each(function () {
                herramienta.getFormValue(formValues, $(this));
            });

            return herramienta.objectToXML(formValues);
        },

        consultarPasajes = function consultarPasajes(parametro) {
            var formData = {
                xjxfun: 'tiquetes2_obtener_viajes',
                xjxr: new Date().getTime(),
                'xjxargs[]': parametro
            };

            return consulta(urlPasajes, formData);
        },

        obtenerPasajes = function obtenerPasajes(response) {
            var $ = cheerio.load(response.body, {
                normalizeWhitespace: false,
                xmlMode: true
            });

            $ = cheerio.load($('cmd:first-child').text());

            return $('tr').map(function () {
                var elemento = $(this),
                    pasaje = {
                        salida: elemento.children(1).text(),
                        operador: elemento.children(2).children().attr('class'),
                        servicio: elemento.children(3).children().attr('class'),
                        tarifaIncreible: elemento.children(4).text(),
                        tarifaChevere: elemento.children(5).text(),
                        tarifaComoda: elemento.children(6).text(),
                        tarifaPrimeraClase: elemento.children(7).text()
                    };

                return homologador.homologarPasaje(pasaje, tramo);
            }).toArray();
        },

        flitrarPasajes = function flitrarPasajes(pasajes) {
            return under(pasajes).filter(function(pasaje) {
                return pasaje.servicio && pasaje.precio();
            });
        },

        manejarFlujoAlterno = function manejarFlujoAlterno(dato) {
            if (typeof dato === 'function') {
                return dato();
            }

            console.log(dato);
            return obtenerListaVacia();
        },

        buscarPasajes = function buscarPasajes() {
            return configurarParametros()
                .then(consultarFormulario)
                .then(obtienerFormulario)
                .then(consultarPasajes)
                .then(obtenerPasajes)
                .then(flitrarPasajes)
                .fail(manejarFlujoAlterno);
        };

    return buscarPasajes();
};

module.exports = bolivariano;