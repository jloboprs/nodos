'use strict';

var q = require('q'),
    _ = require('underscore'),
    fabrica = require('../../fabrica'),
    servicios = require('./servicios.json');


var libreria = function libreria() {
    var randomInt = function randomInt (low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    };

    var agregarHoras= function agregarHoras(fecha, horas) {
        var fechaNueva = new Date(fecha.getTime());
        fechaNueva.setHours(fechaNueva.getHours() + horas);

        return fechaNueva;
    };

    return {
        randomInt: randomInt,
        agregarHoras: agregarHoras,
        constructor: libreria
    };
};

var fabricaPasajeDummy = function fabricaPasaje() {
    var lib = libreria();

    var crear = function crear(parametros) {
        var pasaje = {
            tarifaBase: obtenerPrecio(parametros),
            sillas: obtenerSillas(parametros),
            salida: obtenerSalida(parametros)
        };

        pasaje.llegada = obtenerLlegada(parametros, pasaje.salida);

        _.extend(pasaje, obtenerOperador(parametros));

        return fabrica.pasaje(pasaje);
    };

    var obtenerOperador = function obtenerOperador(parametros) {
        var servicio = parametros.servicio || _.chain(servicios).keys().sample().value();

        var valores = servicios[servicio];

        return {
            operador: { nombre: valores.operador },
            servicio: { nombre: valores.nombre, nivel: valores.nivel }
        };
    };

    var obtenerPrecio = function obtenerPrecio(parametros) {
        return parametros.precio || lib.randomInt(12000, 120000);
    };

    var obtenerSillas = function obtenerSillas(parametros) {
        return parametros.sillas !== false
            ? (parametros.sillas || lib.randomInt(2, 40))
            : undefined;
    };

    var obtenerSalida = function obtenerSalida(parametros) {
        if (parametros.salida) {
            return parametros.salida;
        }

        var hora = lib.randomInt(0, 23);
        return lib.agregarHoras(new Date(), hora);
    };

    var obtenerLlegada = function obtenerLlegada(parametros, salida) {
        if (parametros.duracion === false) {
            return undefined;
        }

        var duracion = parametros.duracion || lib.randomInt(2, 26);

        return lib.agregarHoras(salida, duracion);
    };

    return {
        crear: crear,
        constructor: fabricaPasajeDummy
    };
};

var generadorPasajes = function generadorPasajes() {
    var salida;
    var lib = libreria();
    var fabrica = fabricaPasajeDummy();

    var defecto = function defecto(tramo) {
        var i, pasajes = [];

        for (i = 0; i < 6; i += 2) {
            salida = lib.agregarHoras(tramo.salida, i);
            pasajes.push(fabrica.crear({salida: salida}));
        }

        for (i = 7; i < 24; i += 2) {
            salida = lib.agregarHoras(tramo.salida, i);
            pasajes.push(fabrica.crear({salida: salida, duracion: false, sillas: false}));

            i += 2;
            salida = lib.agregarHoras(tramo.salida, i);
            pasajes.push(fabrica.crear({salida: salida, duracion: false}));

            i += 2;
            salida = lib.agregarHoras(tramo.salida, i);
            pasajes.push(fabrica.crear({salida: salida, sillas: false}));
        }

        return pasajes;
    };

    var requerido = function requerido(tramo, siguiente) {
        if (!(tramo.origen.codigo === 'barranquilla' && tramo.destino.codigo === 'bogota' ||
            tramo.origen.codigo === 'bogota' && tramo.destino.codigo === 'barranquilla')) {
            return siguiente();
        }

        var i, pasajes = [];

        for (i = 0; i < 12; i++) {
            pasajes.push(fabrica.crear({duracion: false, sillas: false}));
        }

        return pasajes;
    };

    var ninguno = function ninguno(tramo, siguiente) {
        if (tramo.origen.codigo !== tramo.destino.codigo) {
            return siguiente();
        }

        return [];
    };

    var obtener = function obtener(tramo) {
        var procesos = [ defecto, requerido, ninguno ];

        var procesar = function procesar() {
            var proceso = procesos.pop();

            return proceso(tramo, procesar);
        };

        return procesar();
    };

    return {
        obtener: obtener,
        constructor: generadorPasajes
    };
};

var dummy = function dummy() {
    var generador = generadorPasajes();

    return function dummy(tramo) {
        var pasajes = generador.obtener(tramo);

        return q.when(pasajes);
    };
};


module.exports = dummy();