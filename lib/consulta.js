'use strict';

var Q = require('q'),
    unirest = require('unirest');

var consulta = function consulta(url, data, metodo, headers, jar) {
    var conf = {
        url: url,
        data: data,
        headers: headers,
        jar: jar,
        metodo: metodo
    },  self = {
        data: function data(dato) {
            conf.data = Object.create(dato);
            return self;
        },
        headers: function headers(dato) {
            conf.headers = Object.create(dato);
            return self;
        },
        jar: function jar(dato) {
            conf.jar = dato || unirest.jar();
            return self;
        },
        consultar: function consultar() {
            var rq = unirest(conf.metodo, conf.url),
                defer = Q.defer();

            if (conf.data) {
                rq.send(conf.data);
            }

            if (conf.headers) {
                rq.headers(conf.headers);
            }

            if (conf.jar) {
                rq.jar(conf.jar);
            }

            rq.end(function (response) {
                if (!response.error && response.ok) {
                    defer.resolve(response);
                } else {
                    var error = new Error("Respuesta no esperada");
                    error.response = response;
                    defer.reject(error);
                }
            });

            return defer.promise;
        }
    };

    if (metodo) {
        return self;
    }

    conf.metodo = data ? 'POST' : 'GET';
    return self.consultar();
};

consulta.post = function post(url, data) {
    return consulta(url, data, 'POST');
};

consulta.get = function get(url) {
    return consulta(url, undefined, 'GET');
};

consulta.head = function get(url) {
    return consulta(url, undefined, 'HEAD');
};

module.exports = consulta;