'use strict';

var q = require('q'),
    under = require('underscore'),
    brasilia = require('./empresas/brasilia'),
    copetran = require('./empresas/copetran'),
    bolivariano = require('./empresas/bolivariano'),

    filtrarErrores = function filtrarErrores(pasajes) {
        return under.reduce(pasajes, function(pasajes, pasaje) {
            if (pasaje.state !== "fulfilled") {
                return pasajes;
            }

            return pasajes.concat(pasaje.value);
        }, []);
    },

    buscarPasajes = function buscarPasajes(tramo) {
        return q.allSettled([brasilia(tramo), bolivariano(tramo), copetran(tramo)])
            .then(filtrarErrores);
    };

module.exports = {
    buscarPasajes: buscarPasajes
};