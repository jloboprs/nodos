'use strict';

var q = require('q'),
    under = require('underscore');

var infinito = function infinito(ms) {
    var defered = q.defer();

    setTimeout(function () {
        defered.resolve(ms);
    }, ms);

    return defered.promise;
};

var timeout = function timeout(ms, objeto) {
    var defered = q.defer(),
        promesa = q(objeto),
        timeoutId = setTimeout(function () {
            defered.reject(promesa);
        }, ms);

    q.when(promesa)
        .then(function (promesa) {
            clearTimeout(timeoutId);
            defered.resolve(promesa);
        })
        .fail(function (motivo) {
            clearTimeout(timeoutId);
            defered.reject(motivo);
        });

    return defered.promise;
};


q.allResolved([1, infinito(10000), 3])
    .timeout(5000)
    .then(function (obj) {
        console.log(obj);
    })
    .fail(function (obj) {
        console.log(obj);
    })

/*
timeout(100, q.allSettled([1, infinito(10000), 3]))
    .then(console.log)
    .fail(function (obj, obj2) {
        console.log(obj2);
    });
*/