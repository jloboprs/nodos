'use strict';
var under = require('underscore'),
    moment = require('moment'),
    nodos = require('../index'),
    bolivariano = require('../empresas/bolivariano'),
    brasilia = require('../empresas/brasilia'),
    copetran = require('../empresas/copetran'),
    dummy = require('../empresas/dummy');

var obtenerFinDeSemana = function obtenerFinDeSemana() {
    var viernes = 5;
    var fecha = new Date();
    var diferencia = viernes - fecha.getDay();

    var adicion = diferencia < 0 ? 7 + diferencia : diferencia;
    return moment(fecha).add(adicion, 'days').toDate();
};

var tramo = {origen: {codigo: 'barranquilla'}, destino: {codigo: 'cartagena'},
    salida: new Date('2014-01-01 00:00:00')};//obtenerFinDeSemana()

var pasajes = dummy(tramo);
//var pasajes = nodos.buscarPasajes(tramo);

pasajes.then(function(pasajes){
    under.each(pasajes, function(pasaje){
        console.log(pasaje);
        console.log(pasaje.calidad());
        console.log('----------------------------------------------');
    });
}).fail(console.log);