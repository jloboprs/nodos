'use strict';

var unirest = require('unirest');

var log = function log(obj, dec) {
    console.log(dec + ': \n');
    console.log(obj);
    console.log('-------------------------\n');
};


unirest.get('http://www.google.com').jar().end(function end(response) {
    log(response.cookies, 'Uno');

    unirest.get('http://www.google.com').jar(response.cookies).end(function end(response) {
        log(response.cookies, 'Dos');
    });
});


var cookies = unirest.jar();
unirest.get('http://www.google.com').jar(cookies).end(function end(response) {
    log(response.cookies, 'Tres');

    unirest.get('http://www.google.com').jar(cookies).end(function end(response) {
        log(response.cookies, 'Cuatro');
    });
});

